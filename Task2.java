import java.util.HashMap;
import java.util.Map;

public class Task2 {

    public static void main(String[] args) {
        Map<String, Integer> studentMap = new HashMap<>();

        studentMap.put("Michael", 20);
        studentMap.put("Jim", 22);
        studentMap.put("Oscar", 21);
        studentMap.put("Pam", 23);

        System.out.println("Students: " + studentMap);

        String nameToCheck = "Michael";
        if (doesNameExist(studentMap, nameToCheck)) {
            System.out.println(nameToCheck + " ada!");
        } else {
            System.out.println(nameToCheck + " tidak ada!");
        }

        String nonExistentName = "Monica";
        if (doesNameExist(studentMap, nonExistentName)) {
            System.out.println(nonExistentName + " ada!");
        } else {
            System.out.println(nonExistentName + " tidak ada!");
        }
    }

    public static boolean doesNameExist(Map<String, Integer> map, String name) {
        return map.containsKey(name);
    }
}
