import java.util.TreeSet;

public class Task5 {

    public static void main(String[] args) {
        TreeSet<Book> bookTreeSet = new TreeSet<>();

        bookTreeSet.add(new Book("Of Mice and Men", "John Steinbeck", "9780140177398"));
        bookTreeSet.add(new Book("Dark Matter", "Blake Crouch", "9781101904220"));
        bookTreeSet.add(new Book("Vicious", "V.E. Schwab", "9780765335340"));
        bookTreeSet.add(new Book("Victorious", "V.E. Schwab", "9780765335340"));

        for (Book book : bookTreeSet) {
            System.out.println(book);
        }
    }

    static class Book implements Comparable<Book> {
        private String title;
        private String author;
        private String ISBN;

        public Book(String title, String author, String ISBN) {
            this.title = title;
            this.author = author;
            this.ISBN = ISBN;
        }

        @Override
        public int compareTo(Book otherBook) {
            return this.title.compareTo(otherBook.title);
        }

        @Override
        public String toString() {
            return "Book{" +
                    "title='" + title + '\'' +
                    ", author='" + author + '\'' +
                    ", ISBN='" + ISBN + '\'' +
                    '}';
        }
    }
}
