import java.util.LinkedList;

public class Task3 {

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();

        linkedList.add(10);
        linkedList.add(20);
        linkedList.add(33);
        linkedList.add(30);

        System.out.println("Sebelum: " + linkedList);

        insertInMiddle(linkedList, 100);

        System.out.println("Sedudah: " + linkedList);
    }

    public static void insertInMiddle(LinkedList<Integer> list, int element) {
        int middleIndex = list.size() / 2;

        list.add(middleIndex, element);
    }
}
