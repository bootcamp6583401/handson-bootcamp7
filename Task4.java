import java.util.Comparator;
import java.util.TreeSet;

public class Task4 {

    public static void main(String[] args) {

        Comparator<String> comparator = new Comparator<String>() {
            public static int order = 0;

            @Override
            public int compare(String o1, String o2) {

                return order++;
            }
        };
        TreeSet<String> treeSet = new TreeSet<>(comparator);

        treeSet.add("Halo");
        treeSet.add("Apa");
        treeSet.add("Kabar");
        treeSet.add("Oke");
        treeSet.add("Lah");

        System.out.println("Sebelum: " + treeSet);

        TreeSet<String> sortedTreeSet = sortTreeSet(treeSet);

        System.out.println("Sesudah: " + sortedTreeSet);
    }

    public static TreeSet<String> sortTreeSet(TreeSet<String> treeSet) {
        TreeSet<String> orderedTreeSet = new TreeSet<>();
        orderedTreeSet.addAll(treeSet);
        return orderedTreeSet;
    }
}
