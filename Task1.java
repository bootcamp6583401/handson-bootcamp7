import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Task1 {

    public static void main(String[] args) {
        ArrayList<String> stringList = new ArrayList<>();

        stringList.add("Satu");
        stringList.add("Dua");
        stringList.add("Tiga");
        stringList.add("Empat");
        stringList.add("Dua");

        System.out.println("Original: " + stringList);

        removeDuplicatesPreserveOrder(stringList);

        System.out.println("Hasil: " + stringList);
    }

    public static void removeDuplicatesPreserveOrder(List<String> list) {
        Set<String> set = new LinkedHashSet<>(list);

        list.clear();

        list.addAll(set);
    }
}
